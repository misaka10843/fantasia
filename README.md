# Sparkle fantasia (Nuxt3)

Sparkle fantasia frontend.

For develop this project, Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn.

## Setup

Make sure to install the dependencies:

```bash
# yarn
yarn install
```

## Development Server

Start the development server on `http://localhost:3000`:

```bash
# yarn
yarn dev
```

## Production

Build the application for production:

```bash
# yarn
yarn build
```

Locally preview production build:

```bash
# yarn
yarn preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.
