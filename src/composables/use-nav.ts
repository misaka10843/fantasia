import {
  mdiHome,
  mdiInformationOutline,
  mdiDownload,
  mdiServer,
  mdiLinkVariant
} from '@mdi/js'

export default function () {
  return [
    {
      text: 'Home',
      icon: mdiHome,
      to: '/'
    },
    {
      text: 'About',
      icon: mdiInformationOutline,
      to: '/about'
    },
    {
      text: 'Download',
      icon: mdiDownload,
      to: '/download'
    },
    {
      text: 'Status',
      icon: mdiServer,
      to: '/status'
    },
    {
      text: 'Links',
      icon: mdiLinkVariant,
      to: '/links'
    }
  ] as const satisfies ReadonlyArray<{ text: string; to: string; icon: string }>
}
