import { defineStore } from 'pinia'

export const useSideMenu = defineStore('sideMenu', () => {
  const isOpen = ref<boolean>(false)
  const openTarget = ref<'left' | 'right'>('left')

  const toggleMenu = () => (isOpen.value = !isOpen.value)
  const closeMenu = () => (isOpen.value = false)

  const setOpenTarget = (target: 'left' | 'right') => {
    openTarget.value = target
  }

  const isOpenLeftMenu = computed((): boolean => {
    return isOpen.value && openTarget.value === 'left'
  })

  const isOpenRightMenu = computed((): boolean => {
    return isOpen.value && openTarget.value === 'right'
  })

  return {
    isOpen,
    isOpenLeftMenu,
    isOpenRightMenu,
    setOpenTarget,
    closeMenu,
    toggleMenu
  }
})
